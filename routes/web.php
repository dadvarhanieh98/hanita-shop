<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/admin')->group(function () {
    Route::prefix('/profile')->group(function () {
        Route::get('/', 'profileController@index');
        Route::post('/','profileController@update');
    });
    Route::prefix('/items')->group(function () {


        Route::get('/', 'stuffController@index')->name('admin_stuffs_preview');

        Route::get('/create', 'stuffController@create')->name('admin_stuffs_add');
        Route::post('/create', 'stuffController@create')->name('admin_stuffs_add_req');

        Route::get('/update', 'stuffController@update')->name('admin_stuffs_edit');
        Route::put('/update', 'stuffController@update')->name('admin_stuffs_edit_req');

        Route::put('/delete', 'stuffController@delete')->name('admin_stuffs_delete');

    });
});





Route::get('/admin' , 'stuffController@update');
Route::post('/admin' , 'stuffController@update');


