<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\stuff;
use Illuminate\Support\Facades\Auth;

class stuffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function delete(Request $req,$id){
        $found_stuff=stuff::FindOrFail($id);
        $found_stuff->delete();
        return redirect()->route('stuff_preview');

    }




   public function create(Request $req){
       if ($req->isMethod('POST')) {
           $file=$req->file('pic');
           $file_name=time().'.'.$file->getClientOriginalExtension();
           $destinationPath=public_path ('/files/suppport/');
           $file->move($destinationPath, $file_name);


           $user = Auth::user();
           if ($user) {


           $label = $req->input("label");
           $price = $req->input("price");
           $desc = $req->input("desc");
           $new_stuff = new stuff ;
               $new_stuff->  userID = $user->id;
           $new_stuff->label = $label;
           $new_stuff->price = $price;
           $new_stuff->desc = $desc;
           $new_stuff->save();

       }

       }
    $stuff = stuff ::all();
       if (Route::has('login')) {
           return view('stuffs.blade');
       }




}
    public function update(Request $req){
        $label= $req-> input("label" );
        $id= $req-> input("id");
        $price= $req -> input("price");
        $desc= $req -> input("desc");
        $new_stuff= stuff::firstOrCreate(["id" => $id]);
        $new_stuff->label=$label;
        $new_stuff->price=$price;
        $new_stuff->desc=$desc;
        $new_stuff->save();
        $stuff = stuff ::all();
        return view('stuffs' , [
            "stuffs" => $stuff,
        ]);

    }
   public function index(Request $req)
   {






       $s = $req ->query("s" , null);
       if($s){

           $stuff=stuff::whereRaw("UPPER('label') LIKE '%" . strtoupper($s)."%'")->get();
       }else {
           $stuff = stuff::all();
       }
       return view('stuffs' , [
           "stuffs" => $stuff,
       ]);
   }
}
