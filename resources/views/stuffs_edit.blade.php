@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST">
                            @method('PUT')
                            @csrf
                            <label>Price</label>
                            <input name="price" value="{{$stuff->price}}" /><br>
                            <label>Label</label>
                            <input name="label" value="{{$stuff->label}}" /><br>
                            <label>Description</label>
                            <input name="desc" value="{{$stuff->desc}}" /><br>
                            <input type="submit" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
