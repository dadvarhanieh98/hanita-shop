@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

{{--                    @foreach($stuffs as $each)--}}
{{--                        <form  method="POST">--}}
{{--                             @method("put")--}}
{{--                            @csrf--}}
{{--                            <label>Price</label>--}}
{{--                            <input name="price" value="{{$stuff-> price}}" /><br>--}}

{{--                            <label>Label</label>--}}
{{--                            <input name="label" value="{{$each -> label}}" /><br>--}}


{{--                            <label>Description</label>--}}
{{--                            <input name="desc" value="{{$each -> desc}}" /><br>--}}

{{--                            <input type="submit">--}}



{{--                            </form >--}}
{{--                    <hr>--}}
{{--                    @endforeach--}}
                    @foreach($stuffs as $each)
                        {{$each->label}}<br>
                        {{$each->price}}<br>
                        {{$each->desc}}<br>
                    @if($each -> user)
                        {{$each->user->name}}<br>
                        @endif
                        <a class="btn btn-primary" href="{{route('stuffs_edit', [$each->id])}}">edit</a>
                        <hr>
                    @endforeach



                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST">
                            @csrf
                            <input name="price" />
                            <input name="label" type="number" />
                            <input name="desc" />
                            <input type="submit" />
                            </form>

                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
